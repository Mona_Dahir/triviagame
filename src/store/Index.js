import Vue from 'vue'
import Vuex from 'vuex'
import { getAllCategories, getQuestions } from '../util/questionsApi'
import router from '../router/'
Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        lastRequest: '',
        difficulty: '',
        categories: [],
        selectedCategoryId: '',
        amountOfQuestions: '',
        chosenAmountOfQuestions: 1,
        questions: [],
        question: {},
        answers: [],
        error: '',
    },
    mutations: {
        setLastRequest: (state, payload)=>{
            state.lastRequest = payload
        },
        setCategories: (state, payload) => {
            state.categories = payload
        },
        setDifficulty: (state, payload) => {
            state.difficulty = payload
        },
        setAmountOfQuestions: (state, payload) => {
            state.amountOfQuestions = payload
        },
        setChosenAmountOfQuestions: (state, payload) => {
            state.chosenAmountOfQuestions = payload
        },
        setSelectedCategoryId: (state, payload) => {
            state.selectedCategoryId = payload
        },
        setQuestions: (state, payload) => {
            state.questions = payload
        },
        setQuestion: (state, payload) => {
            state.question = payload
        },
        setAnswers: (state, payload) => {
            state.answers.push(payload)
        },
        resetAnswers: (state)=>{
            state.answers = []
        },
        setError: (state, payload) => {
            state.error = payload,
                alert(state.error)
        },
    },
    getters: {
        getQuestions: (state) => {
            return state.questions;
        },
        getQuestion: (state) => {
            return state.question;
        },
        getAnswers: (state)=>{
            return state.answers
        },
        getLastRequest: (state)=>{
            return state.lastRequest
        },
    },
    actions: {
        // Gets all categories (id, name) and commits them to the state.categories
        // Else commits error message to error
        async getCategories({ commit }) {
            try {
                const fetchedCategories = await getAllCategories()

                if (fetchedCategories != null) {
                    commit('setCategories', fetchedCategories)
                } else {
                    commit('setError', 'Categories could not be retrieved.')
                }
            } catch (error) {
                commit('setError', error.message)
            }
        },
        
        async startGame({ state, commit }) {
            //check that amount of question is not unvalid
            if (state.chosenAmountOfQuestions > 50 || state.chosenAmountOfQuestions < 1 || isNaN(state.chosenAmountOfQuestions)) {
                return alert('Sorry! Maximum number of questions is 50 and minimum is 1')
            }
            if(state.selectedCategoryId == '') return alert('You need to choose a category')
            // Set chosen options
            const request = {
                category: state.selectedCategoryId,
                difficulty: state.difficulty.toLowerCase(),
                amount: state.chosenAmountOfQuestions
            }
            //Set last request with current reqest (for restarting game with same parameters)
            this.commit('setLastRequest', request)
            
            try {
                // Gets and sets all questions and sets the first question
                const fetchedData = await getQuestions(request)
                const firstQuestion = fetchedData[0];
                commit('setQuestions', fetchedData)
                commit('setQuestion', firstQuestion)

                // route to first question
                router.push(`/questions?q=${0}`);
            } catch (error) {
                commit('setError', error.message)
            }
        },
        // Uses lastRequest to send same reqest to get different questions with the same parameters.
        async restartGame({ commit}){
            try {
                const fetchedData = await getQuestions(this.getters.getLastRequest)   
                const firstQuestion = fetchedData[0];
                commit('resetAnswers')
                commit('setQuestions', fetchedData)
                commit('setQuestion', firstQuestion)
                router.push(`/questions?q=${0}`);
            } catch (error) {
                commit('setError', error.message)
                alert('Something went wrong. Redirecting you to start page')
                router.push('/')
            }
        },
        // Takes in the current question index and increments it to next question
        setNextQuestion({ state, commit }, currentIndex) {
            const index = parseInt(currentIndex);
            try {
                commit('setQuestion', state.questions[index + 1])
                router.push(`/questions?q=${index + 1}`);

            } catch (error) {
                commit('setError', error.message)
            }
        },
     
    }
})