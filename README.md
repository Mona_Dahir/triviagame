# trivia-game

## Heroku
https://trivia-game-vue-js.herokuapp.com/

## Known Issues
**For some reason the data fetched from the Opentdb-API has its correct answers contained inside the incorrect_answers array.** 


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
